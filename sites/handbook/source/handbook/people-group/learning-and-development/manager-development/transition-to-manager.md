---
layout: handbook-page-toc
title: Transitioning to a people manager role
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Transitioning to a people manager role

Your first few months as a manager at GitLab can be exhilarating. They can also be challenging, especially when you need to quickly identify what is important for your success.

Some of the challenges you might face are:

* Navigating changing relationships
* Finding the balance between managing and contributing
* Using delegation effectively
* Making strategic decisions
* Finding resources and support 

[Research by HR consultancy DDI ](https://www.management-issues.com/news/3917/dealing-with-a-promotion-is-as-stressful-as-divorce/) has shown, that your transition may be one of the most demanding life experiences that you will ever endure. However, the satisfaction of leading teams to success makes that journey worthwhile and inspiring.

To reach that success, you will need a network of support, templates, tools, and training. At GitLab, we develop and iterate upon an engaging pathway that enables managers to lead teams to success. This pathway begins with a **[Becoming a GitLab Manager issue](https://gitlab.com/gitlab-com/people-group/Training/-/blob/master/.gitlab/issue_templates/becoming-a-gitlab-manager.md)**. This checklist will ensure that, as a new manager, you have a strong foundation, with all the tactical resources available. The Learning and Development team, your leader, and a peer network of experienced colleagues are ready to support you.

#### Working with a new team

A new team can take many forms:
1. You (the manager) are new to a team that is already in place
2. Your existing team picks up 1 or more new team members
3. Your team becomes smaller and team members have to pick up more tasks

In any of these cases, learning is really important.  You all need to learn about each other and how you will work together.  You all need to learn about the work you need to do as a team.  There may be one team member who knows a lot.  How do you get that knowledge shared?

Here are some ideas to help you and/or your team get up to speed:

##### Rotating Team work
Something to try on a team where 50% may be new and 1 or 2 people have knowledge you want to share.
Try setting up a rotation where team members work together on an issue.  1 of the pair needs to have good knowledge of what to do and the other is the learner.  Every 2-3 days, the pair has to rotate.  The team member who has been on the issue the longest has to go to a new issue and the remaining team member in the pair has to teach the new team member joining the issue what is being done.  This can be really uncomfortable at first and may slow things down, but it will help force knowledge sharing.

##### Shadow people for part of a workday
Have new team members setup time slots to shadow other team members or stable-counterparts for part of a day.  The mentor/shadowee will need to be more vocal in explaining what they are doing.  

#### Finding the balance between contributing and managing 

Let’s take a closer look at just two of the challenges you might face.  The first is finding a balance between contributing and managing.

By doing too much of the team’s work yourself, you may deprive team members of the means to improve themselves and make them overly dependent upon you.  You also risk facing burnout.  One solution is to change the way you contribute to your employees’ efforts.  Instead of doing a piece of work for them, ask them to take small iterative steps.

For example, a team member has a project but has no idea where to start, so they ask you for help. 

You could show them how you would start it, but that could lead to a long-term dependency. Your team may internalize that they should always ask you before starting something.  Instead, try telling them that they should comment on an issue with five ideas by 10 a.m. the next day, and you will respond with your comments/suggestions by 10 a.m. the following day. This may be the same amount of work for you at first, but over time it will shift.  Your team will begin to take smaller steps on their own, and allow you more time to focus on providing guidance and course correction.

#### Navigating the complexity of managing your former peers

If you have been promoted to a manager role, a second challenge you might face is navigating the complexity of managing your former peers. 

The key to making this transition smooth is to be inclusive and communicate with your team and external collaborators.  During your first few weeks as a new manager, try to identify a few small decisions you can make fairly quickly, but defer bigger ones until you’ve been in the role longer and have time to gather input from everyone. For example, set up a new schedule and agenda for team meetings, this is a small change but it clearly establishes your role as manager. 

Also, in the first few weeks, take actions that establish your credibility and indicate how you’ll work as a manager. One of the best ways is to meet with your team, as a group and individually, and share with them your management style or review your Manager [README](/handbook/engineering/readmes/eric-johnson/).

During this type of transition, it’s easy to become overly focused on your former peers.  Don’t forget to build relationships with your new leader and schedule coffee chats with other stakeholders across the organization.

#### The new manager enablement issue, support, and resources 

There are many more challenges that you could face as a new manager.  The goal of this program is to provide a foundation for your success and enable you to overcome those challenges.  

As a manager, you can lead your team to learn, grow and succeed.  Following their progress and acknowledging their development can be the most rewarding aspect of your work. As issues arise, you can lean on a support network and receive guidance from experienced colleagues.  This support consists of recurring Managers' coffee chats, dedicated slack channels, training modules, templates, leaders/mentors, and People Ops Business Partners all ready to answer questions and provide you with guidance.

The **[Becoming a GitLab Manager issue](https://gitlab.com/gitlab-com/people-group/Training/-/blob/master/.gitlab/issue_templates/becoming-a-gitlab-manager.md)** is a launchpad, and it can connect you with crucial information about being a manager at GitLab.  This issue should be assigned to you by your manager as part of your transition to the manager role, however, you can also assign it to yourself.  It is designed to ensure you start your journey as a manager with all the resources training available.   This checklist will evolve as we iterate upon it and continue to build this program.  Your contributions as a new manager are essential, please submit a merge request with ideas for improvement.

If you have questions as you work your way through this checklist please ask them in [#managers](https://gitlab.slack.com/messages/C5Z55R5J5/details/) Slack channel.

#### Create a personalized learning pathway
Take a moment to reflect on where you are in this transition. Then use this [google form](https://forms.gle/WUpxZNG2VcYFQLon9) to add your thoughts about the challenges you face, how you plan to overcome them, and what resources you have found helpful so far. The Learning and Development team will review this feedback to personalize your development pathway as a manager at GitLab.  This may include developing custom training modules, pairing you with a mentor, or providing you with opportunities to practice skills.

**Content Sources:** 

* [How to Manage Your Former Peers, Harvard Business Review](https://hbr.org/2012/12/how-to-manage-your-former-peer)
* [Coping successfully with your transition, Lynda.com](https://www.lynda.com/Business-Skills-tutorials/Coping-successfully-your-transition/119004/132715-4.html)
* [Are You A First-Time Manager? Here Are 5 Essential Tips for Success!, Inc.com](https://www.inc.com/melissa-lamson/5-success-strategies-for-first-time-managers.html)
* [Use This Tactic To Avoid Burnout As A New Manager, American Management Association](https://playbook.amanet.org/training-articles-avoid-burnout-new-manager/)
