---
layout: handbook-page-toc
title: "GitLab with Git Basics Hands On Guide"
description: "This Hands On Guide is designed to walk you through the lab exercises used in the GitLab with Git Basics course."
---
# GitLab with Git Basics Hands On Guide
{:.no_toc}

## GitLab with Git Basics Labs

* [Lab 1- Create a Project and an Issue](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab1.html)
* [Lab 2- Work with Git Locally](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab2.html)
* [Lab 3- Use GitLab to Push Code](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab3.html)
* [Lab 4- Build a gitlab-ci.yml File](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab4.html)
* [Lab 5- Use an Auto DevOps Predefined Template](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab5.html)
* [Lab 6- Use Security Scanning (SAST)](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab6.html)

## Quick Links

Here are some quick links that may be useful when reviewing this Hands On Guide.

* [GitLab with Git Basics Course Description](https://about.gitlab.com/services/education/gitlab-basics/)
* [GitLab Certified Associate Certification Details](https://about.gitlab.com/services/education/gitlab-certified-associate/)

### Suggestions?

If you wish to make a change to the *Hands On Guide for GitLab with Git Basics*, please submit your changes via Merge Request!
