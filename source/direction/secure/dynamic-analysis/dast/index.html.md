---
layout: secure_and_protect_direction
title: "Category Direction - Dynamic Application Security Testing"
description: "Dynamic application security testing (DAST) is a process of testing an application or software product in an operating state. Learn more here!"
canonical_path: "/direction/secure/dynamic-analysis/dast/"
---

- TOC
{:toc}
## Secure

| | |
| --- | --- |
| Stage | [Secure](/direction/secure/) |
| Maturity | [Viable](/direction/maturity/) |
| Content Last Reviewed | `2020-06-04` |

## Description
### Introduction and how you can help
<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

<!--
<EXAMPLE>
Thanks for visiting this category direction page on Snippets in GitLab. This page belongs to the [Editor](/handbook/product/categories/#editor-group) group of the Create stage and is maintained by <PM NAME>([E-Mail](mailto:<EMAIL@gitlab.com>) [Twitter](https://twitter.com/<TWITTER>)).

This direction page is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=snippets) and [epics]((https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=snippets) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for snippets, we'd especially love to hear from you.
</EXAMPLE>
-->

Thank you for visiting this category direction page on Dynamic Application Security Testing (DAST) at GitLab. This page belongs to the Dynamic Analysis group of the Secure stage and is maintained by Derek Ferguson ([dferguson@gitlab.com](mailto:<dferguson@gitlab.com>)).

This direction page is a work in progress and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ADAST) and [epics](https://gitlab.com/groups/gitlab-org/-/epics/2912) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email or on a video call. If you're a GitLab user and have direct knowledge of your need for DAST, we'd especially love to hear from you.

### Overview

Dynamic application security testing (DAST) is a process of testing an application or software product in an operating state. 

> GitLab was recently named as a [Challenger in the 2021 Magic Quadrant for Application Security Testing](https://about.gitlab.com/analysts/gartner-ast21/).

When an application has been deployed and started, DAST connects to the published service via its standard web port and performs a scan of the entire application. It can enumerate pages and verify if well-known attack techniques, like cross-site scripts or SQL injections are possible. DAST will also scan REST APIs using an OpenAPI specification as a guide for testing the API endpoints.

DAST doesn't need to be language specific, since the tool emulates a web client interacting with the application, as an end user would.

As DAST has historically been the domain of security teams, rather than developers, testing the application in its running state has often been overlooked during the development process. Since this type of testing requires an application to be deployed, the attack surface of the running application is usually not evaluated until the application has finished the development cycle and is deployed to a staging server (or worse, to production!). Testing this late in the SDLC means that developers have little to no time to respond to any vulnerabilities are found and trade-offs must be made between fixing vulnerabilities and releasing on time. This can lead to vulnerabilities being released into production either as a calculated risk or with no knowledge of the vulnerability at all. 

We see the area of dynamic application security testing as an ideal collaboration point between established security teams and developers, leading to finding and fixing vulnerabilities earlier in the SDLC and reducing the number of vulnerabilities released to production. By integrating DAST into their pipelines and utilizing review apps, developers can be more conscious of the security impacts their code has on the running application. This awareness can enable them to take initiative and fix these before merging their features into the default branch. For the security team member, being able to create issues for any vulnerability found earlier in the SDLC (either by reviewing pipeline results or running on-demand scans) allows them to take a proactive approach to security, rather than reactive. All of this allows these teams to work together to reduce the overall risk of deploying new code to a production application.

### Goal

Our goal is to provide DAST as part of the standard development process. This means that DAST is executed every time a new commit is pushed to a branch. We also include DAST as part of [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

Since DAST requires a running application, we can provide results for feature branches leveraging [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/), temporary environments that run the modified version of the application. We can also provide results for applications running on other servers, such as staging or development environments, either through a CI/CD pipeline scan or a manually triggered on-demand scan.

DAST results can be consumed in the merge request, where new vulnerabilities are shown. A full report is available in the pipeline details page.

DAST results are also a part of the [Security Dashboard](https://docs.gitlab.com/ee/user/application_security/security_dashboard/), where Security Teams can check the security status of their projects.

We also want to ensure that the production environment is always secure by allowing users to run an on-demand DAST scan on a deployed app even if there is no change in the code. On-demand scans will allow for out-of-band security testing and issue reproduction, without needing any code changes or merge requests to start a scan.

### Roadmap

- [Aggregation of noisy DAST vulnerabilities](https://gitlab.com/gitlab-org/gitlab/-/issues/254043) (Shipped in 14.0)
- [Scheduled on-demand DAST scans](https://gitlab.com/groups/gitlab-org/-/epics/4876)
- [CI pipeline scan UI configuration](https://gitlab.com/gitlab-org/gitlab/-/issues/276957)
- [DAST profiles in CI pipeline scans](https://gitlab.com/gitlab-org/gitlab/-/issues/216514)
- [API Security scanner for DAST](https://gitlab.com/groups/gitlab-org/-/epics/4254)
- [Browser-based scanner for DAST](https://gitlab.com/groups/gitlab-org/-/epics/4248)


## What's Next & Why

Now that the on-demand scan GA has shipped, we want to improve the on-demand DAST experience by adding the ability to schedule on-demand scans, rather than always immediately starting the scan manually. This scheduler will allow for easily setting up recurring scans as well as scheduling one-off scans for a time when fewer people might be using the site you are scanning. The ability to have a DAST schedule will help security team members better integrate their testing schedule into the DevSecOps lifecycle and allow for better communication and workflow between development and security teams.
- [Scheduled on-demand DAST scans](https://gitlab.com/groups/gitlab-org/-/epics/4876)

As a part of increasing the usability of DAST scan jobs in the CI/CD pipeline, we are working on two major improvements to the configuration workflow for these scans. The first major improvement is that we are extending the UI that we built for configuring on-demand DAST scans to be usable for all DAST scans, whether they are run as CI/CD pipeline jobs or as on-demand scans. This UI will help allow you to create DAST configurations within the web UI and give you the YAML code necessary to include in the gitlab-ci.yml file. At that point, configuration is only a copy and paste away. This will allow for fewer mistakes in handcoded YAML files and more confidence in the configuration you are creating. In addition to this this configuration UI, as the second major improvement, we will be introducing the ability to use DAST profiles that have been stored in the database directly in the gitlab-ci.yml configuration. By specifying a profile that you have built in the web-based configuration UI, you no longer have to memorize the complete set of DAST variables for when you want to reconfigure a scan. It is as easy and switching the profile names in the YAML file to another profile that you have saved in the UI.
- [CI pipeline scan UI configuration](https://gitlab.com/gitlab-org/gitlab/-/issues/276957)
- [DAST profiles in CI pipeline scans](https://gitlab.com/gitlab-org/gitlab/-/issues/216514)

Focusing further on the underlying scanner capabilities, we have begun to integrate two new scanners into the DAST analyzer. We are already running a beta of our new browers-based crawling capabilities, but we will expand the browser-based features to include active and passive vulnerabilty scanning. This new browser-based crawler and scanner will provide significantly expanded application coverage for modern web applications, especially where single-page and JavaScript-heavy applications are concerned. Rather than approaching applications only from a proxy perspective, as most existing DAST scanners do, we will be able to work with modern apps in their native environment - the browser. By viewing web applications as complex JavaScript applications, rather than legacy HTML apps, we will be able to find and test more pages, forms, and permutations of the application than a traditional, proxy-only DAST scanner.
- [Browser-based scanner for DAST](https://gitlab.com/groups/gitlab-org/-/epics/4248)

On the other side of the DAST coin, we are integrating the Peach API Security scanner into DAST to give us an immediate and major improvement in DAST API scanning coverage, configuration, and confidence. As soon as the API Security scanner is integrated, we will gain capabilities that will increase the number and type of APIs that we are able to scan. The new scanner allows for specifying API endpoints via Postman collections and HAR files, adding onto the OpenAPI specification we support currently. It also gives access to scan GraphQL and SOAP APIs, rather than being limited to REST APIs. Improved authentication support for more authentication methods is another major improvement that we will gain with the integration of the new scnner. These new scanners will enable DAST to better scan modern single page applications and APIs other than REST, such as GraphQL and SOAP. For the focus on usability of on-demand scans and profiles, we will be working on adding the ability to use the DAST profiles in the CI/CD pipeline scans and creating a scheduler for on-demand scans. 
- [API Security scanner for DAST](https://gitlab.com/groups/gitlab-org/-/epics/4254)

## Maturity Plan
 - [Viable to Complete](https://gitlab.com/groups/gitlab-org/-/epics/2912)
 - [Complete to Lovable](https://gitlab.com/groups/gitlab-org/-/epics/3207)

## Competitive Landscape

- [CA Veracode](https://www.veracode.com/products/dynamic-analysis-dast)
- [Microfocus Fortify WebInspect](https://software.microfocus.com/en-us/products/webinspect-dynamic-analysis-dast/overview)
- [IBM AppScan](https://www.ibm.com/security/application-security/appscan)
- [Rapid7 AppSpider](https://www.rapid7.com/products/appspider)
- [Detectify](https://detectify.com/)

We have an advantage of being able to provide testing results before the app is deployed into the production environment, by using Review Apps. This means that we can provide DAST results for every single commit. The easy integration of DAST early in the software development life cycle is a unique position that GitLab has in the DAST market. Integrating other tools at this stage of the SDLC is typically difficult, at best.

## Analyst Landscape

We want to engage analysts to make them aware of the security features already available in GitLab. They also perform analysis of vendors in the space and have an eye on the future. We will blend analyst insights with what we hear from our customers, prospects, and the larger market as a whole to ensure we’re adapting as the landscape evolves. 

* [2021 Gartner Magic Quadrant: Application Security Testing](https://about.gitlab.com/analysts/gartner-ast21/)
* [Gartner Application Security Testing Reviews](https://www.gartner.com/reviews/market/application-security-testing)
* [2019 Forester State of Application Security](https://www.forrester.com/report/The+State+Of+Application+Security+2019/-/E-RES145135)

## Top Customer Success/Sales Issue(s)

- [Full list of DAST issues with the "customer" label](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=customer&label_name[]=Category%3ADAST)

## Top user issue(s)

- [Full list of DAST issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ADAST)

## Top internal customer issue(s)

- [Full list of DAST issues with the "internal customer" label](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ADAST&label_name[]=internal%20customer)

## Top Vision Item(s)

- [API Security scanner for DAST](https://gitlab.com/groups/gitlab-org/-/epics/4254)
- [Browser-based scanner for DAST](https://gitlab.com/groups/gitlab-org/-/epics/4248)
- [Scheduled on-demand DAST scans](https://gitlab.com/groups/gitlab-org/-/epics/4876)
